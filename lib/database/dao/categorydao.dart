import 'package:floor/floor.dart';
import 'package:myshop/model/category.dart';

@dao
abstract class CategoryDao {

  @Query('SELECT * FROM Category')
  Future<List<Category>> findAllItems();

  @Insert(onConflict: OnConflictStrategy.rollback)
  Future<void> insertItems(Category items);

}