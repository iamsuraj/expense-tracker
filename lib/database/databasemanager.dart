import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:myshop/model/expense.dart';

class DatabaseManager {
  final collection = FirebaseFirestore.instance.collection('Transaction');

  Future<String> addExpenses(
      int amount, String type, String desc, String category) async {
    String id = DateTime.now().millisecondsSinceEpoch.toString();
    try {
      await collection.doc(id).set({
        'amount': amount,
        'date': DateTime.now(),
        'desc': desc,
        'id': id,
        'type': type,
        'user': 'Test',
        'category': category
      });
      log('Date: ' + DateTime.now().toString());
      return 'Success';
    } on Exception catch (exception) {
      log('Exception $exception');
      return exception.toString();
    } catch (e) {
      log('Error $e');
      return e.toString();
    }
  }

  Future<List<Expense>> expenseFilterUsingDate(
      DateTime fromdate, DateTime todate, String user) async {
    try {
      QuerySnapshot querySnapshot = await collection
          .where('date', isGreaterThanOrEqualTo: fromdate)
          .where('date', isLessThanOrEqualTo: todate)
          //.where('user', isEqualTo: user)
          .get();
      log("RangeDocument ${querySnapshot.docs.length}");
      if (querySnapshot.docs.isNotEmpty) {
        return querySnapshot.docs.map((doc) {
          return Expense(doc.get('id'), doc.get('amount'), doc.get('type'),
              doc.get('desc'), doc.get('category'));
        }).toList();
      } else {
        return [];
      }
    } on Exception catch (exception) {
      log('Exception $exception');
      return [];
    } catch (e) {
      log('Error $e');
      return [];
    }
  }

  Future<List<Expense>> expenseFilterUsingTransType(
      String type, String user) async {
    try {
      QuerySnapshot querySnapshot = await collection
          .where('type', isEqualTo: type)
          .where('user', isEqualTo: user)
          .get();
      log("TypeDocument ${querySnapshot.docs.length}");
      if (querySnapshot.docs.isNotEmpty) {
        return querySnapshot.docs.map((doc) {
          return Expense(doc.get('id'), doc.get('amount'), doc.get('type'),
              doc.get('desc'), doc.get('category'));
        }).toList();
      } else {
        return [];
      }
    } on Exception catch (exception) {
      log('Exception $exception');
      return [];
    } catch (e) {
      log('Error $e');
      return [];
    }
  }

  Future<String> deleteExpenses(String id) async {
    try {
      collection.doc(id).delete();
      return 'Success';
    } on Exception catch (exception) {
      log('Exception $exception');
      return exception.toString();
    } catch (e) {
      log('Error $e');
      return e.toString();
    }
  }

  Future<List<Expense>> getAllExpenses(String user) async {
    try {
      QuerySnapshot querySnapshot =
          await collection.where('user', isEqualTo: user).get();
      log("Documents ${querySnapshot.docs.length}");
      if (querySnapshot.docs.isNotEmpty) {
        return querySnapshot.docs.map((doc) {
          return Expense(doc.get('id'), doc.get('amount'), doc.get('type'),
              doc.get('desc'), doc.get('category'));
        }).toList();
      } else {
        return [];
      }
    } on Exception catch (exception) {
      log('Exception $exception');
      return [];
    } catch (e) {
      log('Error $e');
      return [];
    }
  }
}
