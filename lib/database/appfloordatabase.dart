import 'dart:async';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:floor/floor.dart';
import 'package:myshop/database/dao/categorydao.dart';
import 'package:myshop/model/category.dart';

part 'appfloordatabase.g.dart';

@Database(version: 1, entities: [Category])
abstract class AppFloorDatabase extends FloorDatabase{
  CategoryDao get categoryDao;
}