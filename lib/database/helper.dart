import 'appfloordatabase.dart';

class DatabaseHelper {

  static var database;

  static Future<AppFloorDatabase> getDatabase() async {
    if (database != null){
      return database;
    } else {
      database =  await $FloorAppFloorDatabase.databaseBuilder('flutter_db.db').build();
      return database;
    }
  }


}