import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:myshop/database/appfloordatabase.dart';
import 'package:myshop/database/dao/categorydao.dart';
import 'package:myshop/model/category.dart';

import '../database/helper.dart';

class AddCategory extends StatefulWidget {
  const AddCategory({Key? key}) : super(key: key);

  @override
  State<AddCategory> createState() => _AddCategoryState();
}

class _AddCategoryState extends State<AddCategory> {

  final myController = TextEditingController();

  late List<Category> categoryList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future<List<Category>> refreshState() async {
    var database = await DatabaseHelper.getDatabase();
    categoryList = await database.categoryDao.findAllItems();
    return categoryList;
  }


  Future<void> _insertCategory(BuildContext context) async {
    var database = await DatabaseHelper.getDatabase();
    database.categoryDao.insertItems(
        Category(myController.text, myController.text.toLowerCase()));

    myController.clear();

    setState(() {
      refreshState();
    });
  }

  List<Category> catList = [];

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text('Category Master'),
        centerTitle: true,
        elevation: 5.0,
        titleTextStyle: const TextStyle(fontSize: 20.0, wordSpacing: 1.0),
      ),
      body: Center(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextField(
              controller: myController,
              keyboardType: TextInputType.text,
              decoration: const InputDecoration(
                labelText: 'Category',
                labelStyle: TextStyle(color: Colors.blueGrey),
                fillColor: Colors.white,
                contentPadding: EdgeInsets.only(bottom: -5),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.blueGrey,
                    width: 0.5,
                  ),
                ),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
                prefixIcon: Icon(
                  Icons.settings_input_component_outlined,
                  color: Colors.blueGrey,
                  size: 20,
                ),
              ),
            ),
            ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
              onPressed: () {
                _insertCategory(context);
              },
              child: const Text('Insert'),
            ),
            Expanded(
              child: FutureBuilder(
                future: refreshState(),
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Text(snapshot.error.toString());
                  } else if (snapshot.hasData) {
                    catList = snapshot.requireData;
                    return ListView.builder(
                      itemCount: catList.length,
                      itemBuilder: (context, index) {
                        if (catList.isEmpty) {
                          return Column(
                            children: const [
                              Text('No data found'),
                            ],
                          );
                        } else {
                          return Card(
                            child: ListTile(
                              title: Text(catList[index].name.toString()),
                            ),
                          );
                        }
                      },
                    );
                  } else {
                    return Column(
                      children: const [
                        CircularProgressIndicator(),
                      ],
                    );
                  }
                },
              ),
            )
          ],
        ),
      )),
    );
  }

}
