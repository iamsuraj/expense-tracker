
import 'package:floor/floor.dart';

@entity
class Category{

  @PrimaryKey(autoGenerate: true)
  final int? id;
  String? name;
  String? value;

  Category(this.name,  this.value, {this.id});

}