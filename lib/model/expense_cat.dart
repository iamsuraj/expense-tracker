import 'package:flutter/cupertino.dart';

class ExpenseCat{

  final int amount;
  final String type;
  final String desc;
  final String id;

  ExpenseCat(this.id, this.amount, this.type, this.desc);

}