import 'package:flutter/cupertino.dart';

class Expense{

  final int amount;
  final String type;
  final String desc;
  final String id;
  final String category;

  Expense(this.id, this.amount, this.type, this.desc, this.category);

}