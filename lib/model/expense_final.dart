import 'package:flutter/cupertino.dart';

import 'expense_cat.dart';

class ExpenseFinal{

  final String cat;
  final List<ExpenseCat> expenseCatList;

  ExpenseFinal(this.cat, this.expenseCatList);

}