import 'package:flutter/material.dart';
import 'package:myshop/model/expense.dart';

class CustomSearchDelegate extends SearchDelegate {

  List<Expense> expenseList = [];

  CustomSearchDelegate(this.expenseList);

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = '';
        },
        icon: const Icon(Icons.clear),
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () {
        close(context, null);
      },
      icon: const Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    List<Expense> matchQuery = [];
    for (var expense in expenseList) {
      if (expense.type.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(expense);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return Card(
          child: ListTile(
            title: Text(
              result.type.contains('credit')
                  ? '+ ₹${result.amount}'
                  : '- ₹${result.amount}',
              style: TextStyle(
                color: result.type.contains('credit')
                    ? Colors.green
                    : Colors.red,
              ),
            ),
            subtitle: Text(result.desc),
          ),
        );
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    List<Expense> matchQuery = [];
    for (var expense in expenseList) {
      if (expense.type.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(expense);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) {
        var result = matchQuery[index];
        return Card(
          child: ListTile(
            title: Text(
              result.type.contains('credit')
                  ? '+ ₹${result.amount}'
                  : '- ₹${result.amount}',
              style: TextStyle(
                color: result.type.contains('credit')
                    ? Colors.green
                    : Colors.red,
              ),
            ),
            subtitle: Text(result.desc),
          ),
        );
      },
    );
  }
}
