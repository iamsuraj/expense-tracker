import 'dart:developer';

import 'package:flinq/flinq.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:myshop/database/databasemanager.dart';
import 'package:myshop/menu/category_menu.dart';
import 'package:myshop/model/expense_final.dart';
import 'package:myshop/screens/bookmark.dart';

import '../database/helper.dart';
import '../delegate/search.dart';
import '../model/category.dart';
import '../model/expense.dart';
import '../model/expense_cat.dart';

enum Menu { addCategory, addTransType }

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<List<Expense>> _getAllRecords;
  late List<Category> categoryList;
  late DateTime? fromDate, toDate;

  final fromDateController = TextEditingController();
  final toDateController = TextEditingController();

  bool dateField = false;

  var amount = 0;
  var type = '';
  var desc = '';
  var category = '';
  var total = 0;

  @override
  void initState() {
    super.initState();

    getCategory();
    refreshState();
    getTotal();
  }

  void refreshState() async {
    _getAllRecords = DatabaseManager().getAllExpenses('Test');
    getTotal();
  }

  void getRangeFilterState(DateTime from, DateTime to) async {
    _getAllRecords = DatabaseManager().expenseFilterUsingDate(
        from, to, 'Test');

    getTotal();
  }

  void getTransactionFilterState(String type) async {
    _getAllRecords =
        DatabaseManager().expenseFilterUsingTransType(type, 'Test');
    getTotal();
  }

  void getTotal() {
    _getAllRecords.then((value) {
      setState(() {
        total = value.fold(
            0,
            (previousValue, element) =>
                previousValue +
                (element.type.contains('credit')
                    ? element.amount
                    : -element.amount));
      });
    });
  }

  late bool alreadySaved = false;

  List<Expense> expenseList = [];
  List<Expense> expenseBookmarkedList = [];
  List<ExpenseFinal> catWiseExpenseList = [];

  final _formKey = GlobalKey<FormState>();

  void _displayMaterialBanner(BuildContext context, String title) {
    final scaffoldMessenger = ScaffoldMessenger.of(context);
    scaffoldMessenger.showSnackBar(SnackBar(
      elevation: 5.0,
      duration: const Duration(seconds: 1),
      content: Text(title),
      behavior: SnackBarBehavior.floating,
    ));
  }

  Future<void> _showDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        useSafeArea: true,
        builder: (context) {
          return AlertDialog(
            elevation: 5,
            backgroundColor: Colors.blueGrey,
            actionsAlignment: MainAxisAlignment.center,
            title: const Center(
              child: Text(
                'Add Expense Detail',
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Text(
                  'Please provide all the details correctly for correct and accurate result.',
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 10, 5, 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Amount';
                            } else {
                              amount = int.tryParse(value) ?? 0;
                              return null;
                            }
                          },
                          decoration: const InputDecoration(
                            labelText: 'Amount',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white,
                            contentPadding: EdgeInsets.only(bottom: -5),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 0.5,
                              ),
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            prefixIcon: Icon(
                              Icons.monetization_on_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        DropdownButtonFormField(
                          decoration: const InputDecoration(
                            labelText: 'Transaction Type',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white,
                            contentPadding: EdgeInsets.only(bottom: -5),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 0.5,
                              ),
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            prefixIcon: Icon(
                              Icons.type_specimen_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                          items: const [
                            DropdownMenuItem(
                              value: "credit",
                              child: Text("Credit"),
                            ),
                            DropdownMenuItem(
                              value: "debit",
                              child: Text("Debit"),
                            ),
                          ],
                          onChanged: (String? value) {
                            type = value!;
                          },
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        DropdownButtonFormField(
                          decoration: const InputDecoration(
                            labelText: 'Transaction Category',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white,
                            contentPadding: EdgeInsets.only(bottom: -5),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 0.5,
                              ),
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            prefixIcon: Icon(
                              Icons.settings_input_component_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                          // items: const [
                          //   DropdownMenuItem(
                          //     value: "food",
                          //     child: Text("Food"),
                          //   ),
                          //   DropdownMenuItem(
                          //     value: "travel",
                          //     child: Text("Travel"),
                          //   ),
                          // ],

                          items: categoryList.map((Category category) {
                            return DropdownMenuItem(
                              value: category.value?.toLowerCase().toString(),
                              child: Text(category.name.toString()),
                            );
                          }).toList(),
                          onChanged: (String? value) {
                            category = value!;
                          },
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Enter Description';
                            } else {
                              desc = value;
                              return null;
                            }
                          },
                          maxLength: 40,
                          minLines: 1,
                          maxLines: 3,
                          decoration: const InputDecoration(
                            labelText: 'Description',
                            labelStyle: TextStyle(color: Colors.white),
                            fillColor: Colors.white,
                            contentPadding: EdgeInsets.only(bottom: -5),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                                width: 0.5,
                              ),
                            ),
                            floatingLabelBehavior: FloatingLabelBehavior.auto,
                            prefixIcon: Icon(
                              Icons.description_outlined,
                              color: Colors.white,
                              size: 20,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            actions: [
              ElevatedButton(
                onPressed: () {
                  _saveExpense(context);
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                ),
                child: const Text(
                  'Save',
                  style: TextStyle(color: Colors.blueGrey),
                ),
              ),
              ElevatedButton(
                onPressed: () => Navigator.pop(context),
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                ),
                child: const Text(
                  'Dismiss',
                  style: TextStyle(color: Colors.blueGrey),
                ),
              ),
            ],
          );
        });
  }

  void _saveExpense(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      DatabaseManager()
          .addExpenses(amount, type, desc, category)
          .then((result) async {
        if (result.contains('Success')) {
          refreshState();
        }
        Navigator.pop(context);
        _displayMaterialBanner(context, result);
      });
    }
  }

  bool isDataExisted(String id) {
    var data =
        expenseBookmarkedList.where((element) => element.id.contains(id));
    if (data.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text('My Expense'),
        centerTitle: true,
        elevation: 5.0,
        titleTextStyle: const TextStyle(fontSize: 20.0, wordSpacing: 1.0),
        actions: [
          IconButton(
            tooltip: 'Logout',
            onPressed: () {
              _displayMaterialBanner(context, 'Logout Button Pressed');
            },
            icon: const Icon(
              Icons.logout_rounded,
              color: Colors.white,
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Create',
        onPressed: () {
          getCategory();
          _showDialog(context);
        },
        backgroundColor: Colors.blueGrey,
        child: const Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.blueGrey,
        shape: const CircularNotchedRectangle(),
        child: IconTheme(
          data: IconThemeData(color: Theme.of(context).colorScheme.onPrimary),
          child: Row(
            children: [
              PopupMenuButton<Menu>(
                  icon: const Icon(Icons.menu),
                  onSelected: (Menu item) {
                    setState(() {
                      switch (item.index) {
                        case 0:
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const AddCategory()));
                          }
                          break;
                        default:
                          {
                            _displayMaterialBanner(context, 'Not implemented');
                          }
                          break;
                      }
                    });
                  },
                  itemBuilder: (context) => <PopupMenuEntry<Menu>>[
                        const PopupMenuItem<Menu>(
                          value: Menu.addCategory,
                          child: Text('Category Master'),
                        ),
                        const PopupMenuItem<Menu>(
                          value: Menu.addTransType,
                          child: Text('Transaction Master'),
                        ),
                      ]),
              IconButton(
                tooltip: 'Bookmark',
                icon: const Icon(Icons.bookmark),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => BookmarkList(
                          expenseList: expenseBookmarkedList,
                        ),
                      ));
                },
              ),
              IconButton(
                tooltip: 'Search',
                icon: const Icon(Icons.search),
                onPressed: () {
                  showSearch(
                      context: context,
                      delegate: CustomSearchDelegate(expenseList));
                },
              ),
            ],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        refreshState();
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueGrey),
                      ),
                      child: const Text(
                        'Show All',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          if (dateField) {
                            dateField = false;
                          } else {
                            dateField = true;
                          }
                        });
                      },
                      // onPressed: () async {
                      //   final picked = await showDateRangePicker(
                      //     context: context,
                      //     helpText: 'Select date range to filter',
                      //     initialEntryMode: DatePickerEntryMode.calendarOnly,
                      //     initialDateRange: DateTimeRange(start: DateTime.now().subtract(const Duration(days: 2)), end: DateTime.now()),
                      //     lastDate: DateTime.now(),
                      //     firstDate: DateTime.now().subtract(const Duration(days: 7)),
                      //   );
                      //   if (picked != null) {
                      //     getRangeFilterState(picked);
                      //   }
                      // },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueGrey),
                      ),
                      child: const Text(
                        'By date',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        getTransactionFilterState('credit');
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueGrey),
                      ),
                      child: const Text(
                        'By credit',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        getTransactionFilterState('debit');
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blueGrey),
                      ),
                      child: const Text(
                        'By debit',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
              child: Visibility(
                visible: dateField,
                child: Column(
                  children: [
                    TextField(
                        controller: fromDateController,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.calendar_today),
                            labelText: "Enter From Date"),
                        readOnly: true,
                        onTap: () async {
                          fromDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now()
                                  .subtract(const Duration(days: 7)),
                              lastDate: DateTime.now());

                          if (fromDate != null) {
                            if (fromDate!.isAtSameMomentAs(DateTime.now())) {
                              _displayMaterialBanner(
                                  context, 'From Date cannot be current date');
                            } else {
                              String formattedDate =
                                  DateFormat('dd-MM-yyyy').format(fromDate!);
                              setState(() {
                                fromDateController.text =
                                    formattedDate; //set foratted date to TextField value.
                              });
                            }
                          }
                        }),
                    TextField(
                        controller: toDateController,
                        decoration: const InputDecoration(
                            icon: Icon(Icons.calendar_today),
                            labelText: "Enter To Date"),
                        readOnly: true,
                        onTap: () async {
                          toDate = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now()
                                  .subtract(const Duration(days: 7)),
                              lastDate: DateTime.now());

                          if (toDate != null) {
                            String formattedDate =
                                DateFormat('dd-MM-yyyy').format(toDate!);

                            //TODO: Date Validation

                            if (toDate!.isBefore(fromDate!)) {
                              _displayMaterialBanner(
                                  context, 'To Date Smaller Than From Date');
                            } else if (fromDate!.isAtSameMomentAs(toDate!)) {
                              _displayMaterialBanner(
                                  context, 'From Date and To Date are same');
                            } else {
                              setState(() {
                                toDateController.text = formattedDate;
                                getRangeFilterState(fromDate!,toDate!);
                              });
                            }
                          }
                        }),
                  ],
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder<List<Expense>>(
                future: _getAllRecords,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    return Column(
                      children: const [
                        Text('Something went wrong'),
                      ],
                    );
                  } else if (snapshot.hasData) {
                    if (snapshot.data!.isEmpty) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text('No data Found'),
                        ],
                      );
                    } else {
                      expenseList = snapshot.requireData;
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                        child: ListView.builder(
                          itemCount: expenseList.length,
                          itemBuilder: (context, index) {
                            var alreadySaved =
                                isDataExisted(expenseList[index].id);

                            var expense = expenseList[index];

                            if (expenseList.isEmpty) {
                              return Column(
                                children: const [
                                  Text('No data found'),
                                ],
                              );
                            } else {
                              return Dismissible(
                                key: UniqueKey(),
                                background: const Card(
                                  color: Colors.red,
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 16),
                                      child: Icon(Icons.delete),
                                    ),
                                  ),
                                ),
                                secondaryBackground: const Card(
                                  color: Colors.transparent,
                                ),
                                confirmDismiss: (direction) async {
                                  if (direction ==
                                      DismissDirection.startToEnd) {
                                    var result = await DatabaseManager()
                                        .deleteExpenses(expense.id);
                                    if (result.contains('Success')) {
                                      expenseBookmarkedList.removeWhere(
                                          (element) =>
                                              element.id.contains(expense.id));

                                      //expenseList.remove(expense.id);
                                      refreshState();
                                    }

                                    _displayMaterialBanner(
                                        context, 'Deleted ${expense.amount}');

                                    return true;
                                  }
                                },
                                // onDismissed: (_) => getTotal(),
                                child: Card(
                                  child: ListTile(
                                    title: Text(
                                      expense.type.contains('credit')
                                          ? '+ ₹${expense.amount}'
                                          : '- ₹${expense.amount}',
                                      style: TextStyle(
                                        color: expense.type.contains('credit')
                                            ? Colors.green
                                            : Colors.red,
                                      ),
                                    ),
                                    subtitle: Text(
                                        '${expense.category[0].toUpperCase() + expense.category.substring(1).toLowerCase()} - ${expense.desc}'),
                                    trailing: IconButton(
                                      icon: Icon(
                                        alreadySaved
                                            ? Icons.bookmark_added
                                            : Icons.bookmark_add_outlined,
                                      ),
                                      color: Colors.blueGrey,
                                      onPressed: () {
                                        setState(() {
                                          if (alreadySaved) {
                                            expenseBookmarkedList.removeWhere(
                                                (element) => element.id
                                                    .contains(expense.id));
                                          } else {
                                            expenseBookmarkedList.add(Expense(
                                                expense.id,
                                                expense.amount,
                                                expense.type,
                                                expense.desc,
                                                expense.category));
                                          }
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              );
                            }
                          },
                        ),
                      );
                    }
                  } else {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const [
                        CircularProgressIndicator(),
                        Text('Please wait...'),
                      ],
                    );
                  }
                },
              ),
            ),
            Card(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  side: const BorderSide(color: Colors.blueGrey)),
              child: ListTile(
                title: const Text(
                  'Total',
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey),
                ),
                subtitle: Text(
                  total == 0 ? '₹ 0' : '₹ $total',
                  style: const TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                      color: Colors.blueGrey),
                ),
                trailing: const Icon(
                  Icons.money,
                  color: Colors.blueGrey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void getCategory() async {
    var database = await DatabaseHelper.getDatabase();
    categoryList = await database.categoryDao.findAllItems();
  }
}
