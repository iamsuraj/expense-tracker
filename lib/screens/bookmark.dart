import 'dart:developer';

import 'package:flutter/material.dart';

import '../model/expense.dart';

class BookmarkList extends StatefulWidget {
  const BookmarkList({Key? key, required this.expenseList}) : super(key: key);

  final List<Expense> expenseList;

  @override
  State<BookmarkList> createState() => _BookmarkListState();
}

class _BookmarkListState extends State<BookmarkList> {
  @override
  Widget build(BuildContext context) {
    print(widget.expenseList.length);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text('My Bookmark'),
        centerTitle: true,
        elevation: 5.0,
        titleTextStyle: const TextStyle(fontSize: 20.0, wordSpacing: 1.0),
      ),
      body: widget.expenseList.isEmpty
          ? const Center(
              child: Text(
                'No bookmark found',
                style: TextStyle(fontSize: 17),
              ),
            )
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView.builder(
                itemCount: widget.expenseList.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: Text(
                        widget.expenseList[index].type.contains('credit')
                            ? '+ ₹${widget.expenseList[index].amount}'
                            : '- ₹${widget.expenseList[index].amount}',
                        style: TextStyle(
                          color:
                              widget.expenseList[index].type.contains('credit')
                                  ? Colors.green
                                  : Colors.red,
                        ),
                      ),
                      subtitle: Text(widget.expenseList[index].desc),
                    ),
                  );
                },
              ),
            ),
    );
  }
}
